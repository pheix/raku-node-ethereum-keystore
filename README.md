# Shuffle Ethereum keystore with Raku

Raku module to manage Ethereum keystore.

## Quick start

### Keystore decryption

```perl
my $password  = 'node1';
my $decrypted = Node::Ethereum::KeyStore::V3.new(:keystorepath('./data/92745E7e310d18e23384511b50FAd184cB7CF826.keystore')).decrypt_key(:$password);

$decrypted<buf8>.say;
$decrypted<str>.say;

# Buf[uint8]:0x<63 27 35 B6 6A D8 75 10 8D EE F0 39 BE 85 5A AE 7F 70 26 53 FC C2 B2 EF B1 E5 66 6C 13 06 F2 FD>
# 632735b66ad875108deef039be855aae7f702653fcc2b2efb1e5666c1306f2fd
```

### Plain secret encryption

```perl
my $secret     = '632735b66ad875108deef039be855aae7f702653fcc2b2efb1e5666c1306f2fd'
my $password   = 'node1';
my $key_crypto = Node::Ethereum::KeyStore::V3.new.keystore(:$password, :$secret);

$key_crypto<ciphertext>.say;

# b6aa5d0dd018a000b4fd530db7d55b97d251238d37e2b6c0ee110de3bb524e85
```

## License

Module `Node::Ethereum::KeyStore::V3` is free and open source software, so you can redistribute it and/or modify it under the terms of the [The Artistic License 2.0](https://opensource.org/licenses/Artistic-2.0).

## Credits

1. https://github.com/ethereum/go-ethereum/blob/v1.13.5/accounts/keystore/passphrase.go#L200
2. https://pkg.go.dev/github.com/ethereum/go-ethereum/accounts/keystore#DecryptKey
3. https://github.com/ethereum/wiki/wiki/Web3-Secret-Storage-Definition#scrypt
4. https://raku-advent.blog/2023/12/18/dissecting-the-go-ethereum-keystore-files-using-raku-tools/

## Author

Please contact me via [Matrix](https://matrix.to/#/@k.narkhov:matrix.org) or [LinkedIn](https://www.linkedin.com/in/knarkhov/). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
