unit class Node::Ethereum::KeyStore::V3;

use JSON::Fast;
use Net::Ethereum;
use Node::Ethereum::Keccak256::Native;
use Bitcoin::Core::Secp256k1;
use Crypt::LibScrypt:ver<0.0.7+>:auth<zef:knarkhov>;
use Crypt::LibGcrypt:ver<1.0.5+>;
use Crypt::LibGcrypt::Constants;
use Crypt::LibGcrypt::Cipher;
use Crypt::LibGcrypt::Random;
use Crypt::LibGcrypt::Hash;
use Base58;

# use OpenSSL:ver<0.2.1+>;
# use OpenSSL::CryptTools;

constant kdf_dklen = 32;
constant kdf_n     = 262144;
constant kdf_p     = 1;
constant kdf_r     = 8;

has Str $.keystorepath is rw;
has Str $!keystorecontent;
has     $!eth       = Net::Ethereum.new;
has     $!keccak    = Node::Ethereum::Keccak256::Native.new;
has     $!secp256k1 = Bitcoin::Core::Secp256k1.new;
has     $!sha256    = Crypt::LibGcrypt::Hash.new(:algorithm("SHA256"));

method credentials returns Hash {
    my $password ~= ('0' .. 'z').pick(4).join('') for 1..4;

    (my Str $privkey = $!eth.buf2hex(random(32)).lc) ~~ s/^0x//;

    X::AdHoc.new(:payload('not ECDSA private key')).throw unless $!secp256k1.verify_private_key(:$privkey);

    return {
        password   => $password,
        privatekey => $privkey
    };
}

method save(Hash :$keystore, Str :$path = $!keystorepath, Bool :$overwrite = False) returns Bool {
    X::AdHoc.new(:payload('blank keystore')).throw unless $keystore.keys.elems;

    X::AdHoc.new(:payload('keystore file exists')).throw if
        !$overwrite && $path.IO.e && $path.IO.f;

    $!keystorecontent = to-json($keystore);

    return $path.IO.spurt($!keystorecontent);
}

method keystore(Str :$password!, Str :$secret!, Hash :$kdfparams, Bool :$debug = False, Bool :$tron = False) returns Hash {
    X::AdHoc.new(:payload('blank password')).throw unless $password.chars;

    X::AdHoc.new(:payload('not ECDSA private key')).throw unless $!secp256k1.verify_private_key(:privkey($secret));

    my buf8 $secretbuf8;

    try {
        $secretbuf8 = $!eth.hex2buf($secret);

        CATCH {
            default {
                X::AdHoc.new(:payload(sprintf("secret %s hex to buffer parse failure: %s %s", $secret, .^name, .Str))).throw;
            }
        }
    }

    my $kdf_n     = $kdfparams<n> // kdf_n;
    my $kdf_p     = $kdfparams<p> // kdf_p;
    my $kdf_r     = $kdfparams<r> // kdf_r;
    my $kdf_dklen = $kdfparams<dklen> // kdf_dklen;

    my buf8 $saltbuf    = scrypt-salt(32);
    my buf8 $derivedkey = scrypt-scrypt($password, $saltbuf, $kdf_n, $kdf_r, $kdf_p).subbuf(0, $kdf_dklen);

    X::AdHoc.new(:payload('derived key generation failure')).throw unless $derivedkey && $derivedkey.bytes;

    my buf8 $iv = random(16);

    X::AdHoc.new(:payload('iv generation failure')).throw unless $iv && $iv.bytes == 16;

    # my buf8 $ciphertext = encrypt($secretbuf8, :aes128ctr, :$iv, :key($derivedkey.subbuf(0, 16)));

    my $gcrypt = Crypt::LibGcrypt::Cipher.new(:algorithm(GCRY_CIPHER_AES), :key($derivedkey.subbuf(0, 16)), :mode('CTR'), :ctr($iv));
    my buf8 $ciphertext = $gcrypt.encrypt($secretbuf8, :bin(True));

    X::AdHoc.new(:payload('ciphertext generation failure')).throw unless $ciphertext && $ciphertext.bytes;

    my buf8 $mac = $!keccak.keccak256(:msg($derivedkey.subbuf(*-16).push($ciphertext)));

    (my Str $ciphertext_s = $!eth.buf2hex($ciphertext).lc) ~~ s/^0x//;
    (my Str $saltbuf_s    = $!eth.buf2hex($saltbuf).lc)    ~~ s/^0x//;
    (my Str $iv_s         = $!eth.buf2hex($iv).lc)         ~~ s/^0x//;
    (my Str $mac_s        = $!eth.buf2hex($mac).lc)        ~~ s/^0x//;

    my $pubkey = $!secp256k1.compressed_public_key(:pubkey($!secp256k1.create_public_key(:privkey($secret))), :cmp(False));

    X::AdHoc.new(:payload('public key length')).throw unless $pubkey && $pubkey.bytes == 65;

    my $addrbuf = $!keccak.keccak256(:msg($pubkey.subbuf(1, *))).subbuf(*-20);
    my $addrhex = $!eth.buf2hex($addrbuf).lc;

    return {
        crypto => {
            cipher       => 'aes-128-ctr',
            ciphertext   => $ciphertext_s,
            cipherparams => {
                iv => $iv_s,
            },
            kdf       => 'scrypt',
            kdfparams => {
                dklen => $kdf_dklen,
                n     => $kdf_n,
                p     => $kdf_p,
                r     => $kdf_r,
                salt  => $saltbuf_s
            },
            mac => $mac_s
        },
        address     => $addrhex,
        $tron ?? %(tronaddress => self!convert_to_tron(:address($addrhex))) !! %(),
        id          => self!UUIDv4,
        version     => 3,
        $debug ?? %(debug => {
            secret                => $secret,
            public_key            => $!eth.buf2hex(buf8.new($!secp256k1.key)).lc,
            compressed_public_key => $!eth.buf2hex($pubkey).lc,
            keccak                => $!eth.buf2hex($!keccak.keccak256(:msg($pubkey.subbuf(1, *)))).lc,
        }) !! %(),
    };
}

method decrypt_key(Str :$password!, Bool :$cache = True) returns Hash {
    X::AdHoc.new(:payload('can not load keystore')).throw unless
        $!keystorepath && $!keystorepath.IO.f;

    $!keystorecontent = $!keystorepath.IO.slurp unless $cache && $!keystorecontent;

    X::AdHoc.new(:payload('no keystore data')).throw unless $!keystorecontent.chars;

    my $keystore = {};

    try {
        $keystore = from-json($!keystorecontent);

        CATCH {
            default {
                X::AdHoc.new(:payload(sprintf("keystore %s parse failure: %s %s", $!keystorepath, .^name, .Str))).throw;
            }
        }
    }

    X::AdHoc.new(:payload('unsupported keystore version')).throw unless ($keystore<version>:exists) && $keystore<version> == 3;

    X::AdHoc.new(:payload('blank crypto member')).throw unless ($keystore<crypto>:exists) &&
        $keystore<crypto> ~~ Hash &&
            $keystore<crypto>.keys.elems;

    return self.decrypt_data(:crypto($keystore<crypto>), :$password);
}

method decrypt_data(Hash :$crypto!, Str :$password!) returns Hash {
    X::AdHoc.new(:payload('missed crypto details')).throw unless $crypto.keys;

    X::AdHoc.new(:payload('unknown encryption, only aes-128-ctr is supported')).throw unless
        ($crypto<cipher>:exists) && $crypto<cipher> eq 'aes-128-ctr';

    my buf8 $iv         = $!eth.hex2buf($crypto<cipherparams><iv>);
    my buf8 $mac        = $!eth.hex2buf($crypto<mac>);
    my buf8 $ciphertext = $!eth.hex2buf($crypto<ciphertext>);
    my buf8 $derivedkey = self!get_kdf_key(:$crypto, :$password);
    my buf8 $calcmacbuf = $derivedkey.subbuf(*-16).push($ciphertext);

    X::AdHoc.new(:payload('calculated mac does not equal to given')).throw unless
        $!eth.buf2hex($mac) eq $!eth.buf2hex($!keccak.keccak256(:msg($calcmacbuf)));

    # my buf8 $secret = decrypt($ciphertext, :aes128ctr, :iv($iv), :key($derivedkey.subbuf(0, 16)));
    my $gcrypt = Crypt::LibGcrypt::Cipher.new(:algorithm(GCRY_CIPHER_AES), :key($derivedkey.subbuf(0, 16)), :mode('CTR'), :ctr($iv));

    my buf8 $secret = $gcrypt.decrypt($ciphertext, :bin(True));
    (my $secretstr  = $!eth.buf2hex($secret).lc) ~~ s/^0x//;

    return {
        buf8 => $secret,
        str  => $secretstr
    };
}

method !get_kdf_key(Hash :$crypto!, Str :$password!) returns buf8 {
    X::AdHoc.new(:payload('no KDF parameters')).throw unless
        ($crypto<kdfparams>:exists) &&
        ($crypto<kdfparams><salt>:exists) &&
        ($crypto<kdfparams><n>:exists) &&
        ($crypto<kdfparams><r>:exists) &&
        ($crypto<kdfparams><p>:exists) &&
        ($crypto<kdfparams><dklen>:exists);

    my $kdfparams = $crypto<kdfparams>;

    my buf8 $saltbuf = $!eth.hex2buf($kdfparams<salt>);
    my buf8 $hashbuf = scrypt-scrypt($password, $saltbuf, $kdfparams<n>, $kdfparams<r>, $kdfparams<p>);

    return $hashbuf.subbuf(0, $kdfparams<dklen>);
}

# https://github.com/skinkade/crypt-random/blob/c1bf9393ab736ac5e5de2d0c3f56078c178cc071/lib/Crypt/Random/Extra.pm6#L9
method !UUIDv4 returns Str {
    my buf8 $buf = random(16);

    $buf[6] +|= 0b01000000;
    $buf[6] +&= 0b01001111;
    $buf[8] +|= 0b10000000;
    $buf[8] +&= 0b10111111;

    return (:256[$buf.values].fmt("%32.32x") ~~ /(........)(....)(....)(....)(............)/).join("-").lc;
}

method !convert_to_tron(Str :$address!) returns Str {
    return unless $address ~~ /^ '0x'(<xdigit> ** 40) $/;

    my $tronhex = sprintf("41%s", ~$0);

    my $sha256_0 = $!sha256.write($!eth.hex2buf($tronhex)).hex(:reset);
    my $sha256_1 = $!sha256.write($!eth.hex2buf($sha256_0)).hex(:reset);

    return unless $sha256_1 && $sha256_1.chars == 64;

    my $addchecksum = sprintf("%s%s", $tronhex, $sha256_1.substr(0, 8));

    return Base58::encode($!eth.hex2buf($addchecksum));
}
