use v6.d;
use Test;

use JSON::Fast;
use Node::Ethereum::KeyStore::V3;

constant kspath  = './data/92745E7e310d18e23384511b50FAd184cB7CF826.keystore';
constant address = '0x92745e7e310d18e23384511b50fad184cb7cf826';
constant secret  = '632735b66ad875108deef039be855aae7f702653fcc2b2efb1e5666c1306f2fd';

plan 4;

use-ok 'Node::Ethereum::KeyStore::V3', 'used ok';

subtest {
    plan 5;

    my $ksobj = Node::Ethereum::KeyStore::V3.new;

    throws-like {
        $ksobj.decrypt_key(:crypto({}), :password('qwerty'));
    },
    Exception,
    message => /^'can not load keystore'$/,
    'throws exception on null keystore';

    my $decrypted = Node::Ethereum::KeyStore::V3.new(:keystorepath(kspath)).decrypt_key(:password('node1'));

    ok $decrypted ~~ Hash, 'decryption';
    ok ($decrypted<buf8>:exists) && $decrypted<buf8>.defined && $decrypted<buf8>.bytes, 'secret buffer';
    ok ($decrypted<str>:exists) && $decrypted<str>.defined && $decrypted<str>.chars, 'secret string';
    is $decrypted<str>, secret, 'secret is validated';
}, 'decrypt';

subtest {
    plan 13;

    my Str $keystorepath = './data/tmp.json';

    my $ksobj    = Node::Ethereum::KeyStore::V3.new(:$keystorepath);
    my $keystore = $ksobj.keystore(:password('node1'), :secret(secret));

    ok $keystore ~~ Hash && $keystore<crypto> ~~ Hash, 'crypto struct';
    is $keystore<version>, 3, 'version';
    is $keystore<address>, address, 'ethereum address';

    # https://github.com/masukomi/UUID-V4/blob/2c2b2a52091ec764f53ac1269512a1b571c6931a/lib/UUID/V4.rakumod#L61
    my $uuid_regexp = /^ <[0..9 a..f A..F]> ** 8 "-"
					   [ <[0..9 a..f A..F]> ** 4 "-" ] ** 2
					     <[8 9 a b A B]><[0..9 a..f A..F]> ** 3 "-"
					     <[0..9 a..f A..F]> ** 12 $/;

    ok $keystore<id> ~~ $uuid_regexp, sprintf("v4 UUID %s", $keystore<id>);

    my $decrypted = $ksobj.decrypt_data(:crypto($keystore<crypto>), :password('node1'));

    ok $decrypted ~~ Hash, 'decryption';
    is $decrypted<str>, secret, 'secret is validated';

    ok $ksobj.save(:$keystore, :overwrite(True)), 'save keystore';

    is-deeply $keystore, from-json($keystorepath.IO.slurp), 'verify JSON data';
    ok unlink($keystorepath), 'delete tmp keystore file';

    throws-like {
        $ksobj.save
    },
    Exception,
    message => /^'blank keystore'$/,
    'throws exception on null keystore';

    throws-like {
        $ksobj.save(:$keystore, :path(kspath))
    },
    Exception,
    message => /^'keystore file exists'$/,
    'throws exception on keystore file exists';

    my $creds;

    lives-ok { $creds = $ksobj.credentials }, 'create credentials';

    $keystore = $ksobj.keystore(:password($creds<password>), :secret($creds<privatekey>), :tron(True));

    is $keystore.keys.elems, 5, 'keystore from credentials';

    diag($keystore<address>);
    diag($keystore<tronaddress>);
}, 'encrypt';

subtest {
    plan 1;

    my $ksobj    = Node::Ethereum::KeyStore::V3.new;
    my $keystore = $ksobj.keystore(:password('node1'), :secret(secret), :debug(True));

    ok $keystore ~~ Hash &&
       $keystore<crypto> ~~ Hash &&
       $keystore<debug> ~~ Hash &&
       $keystore<debug>.keys == 4, 'crypto struct with debug';

    diag(to-json($keystore<debug>));
    diag($keystore<address>);
}, 'debug data';

done-testing;
