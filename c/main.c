#include <stdio.h>
#include <gcrypt.h>
#include <openssl/conf.h>
#include <openssl/bio.h>
#include <openssl/buffer.h>
#include <openssl/evp.h>

void handle_errors(int step);

#define BUFLEN 32
#define KEYLEN 16

static char privatKey[KEYLEN] = {0xFA, 0xC1, 0x92, 0xCE, 0xB5, 0xFD, 0x77, 0x29, 0x06, 0xBE, 0xA3, 0xE1, 0x18, 0xA6, 0x9E, 0x8B};
static char cipherTxt[BUFLEN] = {0xD1, 0x72, 0xBF, 0x74, 0x3A, 0x67, 0x4D, 0xA9, 0xCD, 0xAD, 0x04, 0x53, 0x4D, 0x56, 0x92, 0x6E, 0xF8, 0x35, 0x85, 0x34, 0xD4, 0x58, 0xFF, 0xFC, 0xCD, 0x4E, 0x6A, 0xD2, 0xFB, 0xDE, 0x47, 0x9C};
static char iv[KEYLEN] = {0x83, 0xDB, 0xCC, 0x02, 0xD8, 0xCC, 0xB4, 0x0E, 0x46, 0x61, 0x91, 0xA1, 0x23, 0x79, 0x1E, 0x0E};

/* https://cboard.cprogramming.com/c-programming/105743-how-decrypt-encrypt-using-libgcrypt-arc4.html#post937511 */

static void aesGCrypt(int gcry_mode, char * iniVector)
{
    #define GCRY_CIPHER GCRY_CIPHER_AES128

    gcry_error_t     gcryError;
    gcry_cipher_hd_t gcryCipherHd;
    size_t           index;

    size_t keyLength = gcry_cipher_get_algo_keylen(GCRY_CIPHER);
    size_t blkLength = gcry_cipher_get_algo_blklen(GCRY_CIPHER);
    char * outBuffer = malloc(BUFLEN);

    gcryError = gcry_cipher_open(
        &gcryCipherHd,       // gcry_cipher_hd_t *
        GCRY_CIPHER,         // int
        gcry_mode,           // int
        GCRY_CIPHER_SECURE); // https://www.gnupg.org/documentation/manuals/gcrypt/Working-with-cipher-handles.html

    if (gcryError)
    {
        printf("gcry_cipher_open failed:  %s/%s\n",
               gcry_strsource(gcryError),
               gcry_strerror(gcryError));
        return;
    }

    gcryError = gcry_cipher_setkey(gcryCipherHd, privatKey, KEYLEN);

    if (gcryError)
    {
        printf("gcry_cipher_setkey failed:  %s/%s\n",
               gcry_strsource(gcryError),
               gcry_strerror(gcryError));
        return;
    }

    gcryError = gcry_cipher_setctr(gcryCipherHd, iniVector, blkLength);

    if (gcryError)
    {
        printf("gcry_cipher_setctr failed:  %s/%s\n",
               gcry_strsource(gcryError),
               gcry_strerror(gcryError));
        return;
    }

    gcryError = gcry_cipher_decrypt(
        gcryCipherHd, // gcry_cipher_hd_t
        outBuffer,    // void *
        BUFLEN,       // size_t
        cipherTxt,    // const void *
        BUFLEN);      // size_t

    if (gcryError)
    {
        printf("gcry_cipher_decrypt failed:  %s/%s\n",
               gcry_strsource(gcryError),
               gcry_strerror(gcryError));
        return;
    }

    printf("gcry_mode = %s\n", gcry_mode == GCRY_CIPHER_MODE_CTR ? "CTR" : "undef");
    printf("keyLength = %d\n", (int)keyLength);
    printf("blkLength = %d\n", (int)blkLength);

    printf("aesSymKey = ");
    for (index = 0; index<KEYLEN; index++)
        printf("%02X ", (unsigned char)privatKey[index]);
    printf("\n");

    printf("iniVector = ");
    for (index = 0; index<KEYLEN; index++)
        printf("%02X ", (unsigned char)iniVector[index]);
    printf("\n");

    printf("cipherTxt = ");
    for (index = 0; index<BUFLEN; index++)
        printf("%02X ", (unsigned char)cipherTxt[index]);
    printf("\n");

    printf("plainText = ");
    for (index = 0; index<BUFLEN; index++)
        printf("%02X ", (unsigned char)outBuffer[index]);
    printf("\n");

    // clean up after ourselves
    gcry_cipher_close(gcryCipherHd);
    free(outBuffer);
}

/* https://stackoverflow.com/questions/47093199/openssl-aes-decryption-in-c */

static void aesOpenSSL(char * iniVector)
{
    int len;
    int outBuffer_len;
    EVP_CIPHER_CTX *ctx;

    OpenSSL_add_all_algorithms();

    ctx = EVP_CIPHER_CTX_new();

    char * outBuffer = malloc(BUFLEN);

    if (1 != EVP_DecryptInit_ex(ctx, EVP_aes_128_ctr(), NULL, (char *)&privatKey, (char *)&iv)) handle_errors(1);
    if (1 != EVP_DecryptUpdate(ctx, outBuffer, &len, (char *)&cipherTxt, BUFLEN)) handle_errors(2);

    outBuffer_len = len;

    if (1 != EVP_DecryptFinal_ex(ctx, outBuffer + len, &len)) handle_errors(3);

    outBuffer_len += len;

    EVP_CIPHER_CTX_free(ctx);

    printf("plainText = ");
    for (int index = 0; index<BUFLEN; index++)
        printf("%02X ", (unsigned char)outBuffer[index]);
    printf("\n");
}

int main(void)
{
    aesGCrypt(GCRY_CIPHER_MODE_CTR, (char *)&iv);
    aesOpenSSL((char *)&iv);
}

void handle_errors(int step) {
    fprintf(stderr, "error at step %d", step);
}
